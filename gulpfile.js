/**
 * @file Gulp main configuration file.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');

const scssTask = require('./config/scss');
const cssTask = require('./config/css');
const commonjsTask = require('./config/commonjs');
const jsTask = require('./config/js');
const assetsTask = require('./config/assets');
const sassdocTask = require('./config/sassdoc');
const watchTask = require('./config/watch');
const serveTask = require('./config/serve');

// Generate css files and source maps:
gulp.task('scss', scssTask);
// Add vendor prefixes to the compiled css:
gulp.task('css', ['scss'], cssTask);
// Copy layout templates and assets to the build directory:
gulp.task('assets', assetsTask);
// Generate docs for scss:
gulp.task('sassdoc', sassdocTask);
// Compile es2015 modules to commonjs:
gulp.task('commonjs', commonjsTask);
// Make a bundle from commonjs modules:
gulp.task('js', ['commonjs'], jsTask);
// Build all assets:
gulp.task('build', ['assets', 'css', 'sassdoc', 'commonjs', 'js']);
// Watch for changes and rebuild assets:
gulp.task('watch', watchTask);
// Serve assets:
gulp.task('serve', ['watch'], serveTask);
// Default task:
gulp.task('default', ['build']);
