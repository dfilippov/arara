/**
 * @file serve gulp task. Serves the assets.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const serve = require('gulp-serve');

const config = require('./environment');

module.exports = serve([
  config.paths.build,
]);
