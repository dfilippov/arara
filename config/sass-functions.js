/**
 * @file Custom Sass functions.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const imageUrl = require('./sass-functions/image-url');
const imageSize = require('./sass-functions/image-size');
const imageWidth = require('./sass-functions/image-width');
const imageHeight = require('./sass-functions/image-height');
const inlineImage = require('./sass-functions/inline-image');

module.exports = {
  'image-url($filename)': imageUrl,
  'image-size($filename)': imageSize,
  'image-width($filename)': imageWidth,
  'image-height($filename)': imageHeight,
  'inline-image($filename)': inlineImage,
};
