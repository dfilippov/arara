/**
 * @file watch gulp task. Watches for changes and rebuilds assets.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');
const liveReload = require('gulp-livereload');

const config = require('./environment');

module.exports = () => {
  liveReload.listen();
  gulp.watch(config.paths.glob.images, ['scss', 'assets']);
  gulp.watch(config.paths.glob.scss, ['scss', 'css']);
  gulp.watch(config.paths.glob.js, ['commonjs', 'js']);
  gulp.watch(config.paths.glob.layout, ['assets']);
};
