/**
 * @file Global environment configuration.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const srcDir = 'src';
const buildDir = 'build';

module.exports = {
  paths: {
    build: `${buildDir}/`,
    imagesHttp: '../assets/images/',
    sassDoc: 'sassdoc/',
    images: `${srcDir}/assets/images/`,
    favicons: `${srcDir}/assets/favicons/`,
    scss: `${srcDir}/scss/`,
    glob: {
      assets: `${srcDir}/assets/**/*.*`,
      images: `${srcDir}/assets/images/**/*.+(jpg|png|gif|svg)`,
      js: `${srcDir}/js/**/*.js`,
      layout: `${srcDir}/layout/**/*.+(htm|html)`,
      scss: `${srcDir}/scss/**/*.scss`,
    },
    dest: {
      assets: `${buildDir}/assets`,
      commonjs: `${buildDir}/commonjs`,
      css: `${buildDir}/css/`,
      js: `${buildDir}/js`,
      glob: {
        commonjs: `${buildDir}/commonjs/*.js`,
        css: `${buildDir}/css/**/*.css`,
      },
    },
  },
  env: process.env.ENV || 'dev',
};
