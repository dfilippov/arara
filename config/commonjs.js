/**
 * @file commonjs gulp task. Compiles es2015 modules to commonjs modules.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');
const babel = require('gulp-babel');
const gulpif = require('gulp-if');
const sourceMaps = require('gulp-sourcemaps');

const config = require('./environment');

module.exports = (callback) => {
  gulp.src(config.paths.glob.js)
    .pipe(gulpif(config.env === 'dev', sourceMaps.init()))
    .pipe(babel({
      presets: ['babel-preset-env'],
      plugins: ['transform-es2015-modules-commonjs'],
    }))
    .pipe(sourceMaps.write())
    .pipe(gulp.dest(config.paths.dest.commonjs))
    .on('end', callback);
};
