/**
 * @file css gulp task. Adds vendor prefixes to the compiled css.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');
const autoprefixer = require('autoprefixer');
const ieKeyframes = require('postcss-mq-keyframes');
const gulpif = require('gulp-if');
const sourceMaps = require('gulp-sourcemaps');
const filter = require('gulp-filter');
const liveReload = require('gulp-livereload');
const cssmin = require('gulp-clean-css');
const postcss = require('gulp-postcss');
const config = require('./environment');


module.exports = () => {
  gulp.src(config.paths.dest.glob.css)
    .pipe(gulpif(config.env === 'dev', sourceMaps.init({ loadMaps: true })))
    .pipe(cssmin())
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 2 versions', 'Android > 4'],
        cascade: false,
      }),
      ieKeyframes,
    ]))
    .pipe(sourceMaps.write('.'))
    .pipe(gulp.dest(config.paths.dest.css))
    // Filter out changes in the source maps, because the last
    // causing a long sequence of page reloads:
    .pipe(filter(config.paths.dest.glob.css))
    .pipe(liveReload());
};
