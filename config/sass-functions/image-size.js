const sass = require('node-sass');
const path = require('path');
const imageSize = require('image-size');
const config = require('../environment');

/**
 * Provides image-size($filename) custom Sass function.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 * @requires image-size
 * @param {string} fileName - An image filename, including extension, relative to
 *    config.paths.images directory.
 * @param {function} done - A callback function to invoke on async completion. Receives
 *    a sass list, containing the image width and height (or 0,0 in the case of error).
 */
module.exports = (fileName, done) => {
  imageSize(path.join(config.paths.images, fileName.getValue()), (err, dim) => {
    const list = new sass.types.List(2);
    let width = 0;
    let height = 0;

    if (!err) {
      width = dim.width;
      height = dim.height;
    } else {
      console.error(err.toString());
    }

    list.setValue(0, new sass.types.Number(width, 'px'));
    list.setValue(1, new sass.types.Number(height, 'px'));
    done(list);
  });
};
