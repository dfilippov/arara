const sass = require('node-sass');
const path = require('path');
const dataURI = require('datauri').promise;
const config = require('../environment');

/**
 * Provides image-url($filename) custom Sass function.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 * @requires datauri
 * @param {string} fileName - An image filename, including extension, relative to
 *    config.paths.images directory.
 * @param {function} done - A callback function to invoke on async completion. Receives
 *    a string with embeded contents of an image.
 */
module.exports = (fileName, done) => {
  dataURI(path.join(config.paths.images, fileName.getValue()))
    .then(content => done(new sass.types.String(`url(${content})`)))
    .catch(err => {
      console.error(err.toString());
      done(new sass.types.String('none'));
    });
};
