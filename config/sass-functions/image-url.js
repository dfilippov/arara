const sass = require('node-sass');
const config = require('../environment');

/**
 * Provides image-url($filename) custom Sass function.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 * @param {string} fileName - An image filename, including extension, relative to
 *    ENV.imagesPath directory.
 * @return {String} - A string with image URL, based on the
 *    config.paths.imagesHttp value.
 */
module.exports = (fileName) => {
  const fileNameValue = fileName.getValue();
  const httpPath = [
    config.paths.imagesHttp,
    fileNameValue,
  ].join('/').replace(/\/\//g, '/');
  return new sass.types.String(`url(${httpPath})`);
};
