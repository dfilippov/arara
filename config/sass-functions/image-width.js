const imageSize = require('./image-size');

/**
 * Provides image-width($filename) custom Sass function.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 * @requires ./image-size
 * @param {string} fileName - An image filename, including extension, relative to
 *    ENV.imagesPath directory.
 * @param {function} done - A callback function to invoke on async completion. Receives
 *    a string width property value of the image.
 */
module.exports = (fileName, done) => {
  imageSize(fileName, (dimensions) => {
    done(dimensions.getValue(0));
  });
};
