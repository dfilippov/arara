/**
 * @file js gulp task. Creates a bundle from commonjs modules with browserify.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 * @todo fix source maps
 */

const gulp = require('gulp');
// const gulpif = require('gulp-if');
// const sourceMaps = require('gulp-sourcemaps');
const through2 = require('through2');
const browserify = require('browserify');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const liveReload = require('gulp-livereload');

const config = require('./environment');

module.exports = () => {
  gulp.src(config.paths.dest.glob.commonjs)
    // .pipe(gulpif(config.env === 'dev', sourceMaps.init({ loadMaps: true })))
    .pipe(through2.obj((file, enc, next) => {
      const fileTemp = file;
      browserify(fileTemp, {
        // debug: true,
        basedir: config.paths.dest.commonjs,
      }).bundle((err, res) => {
        fileTemp.contents = res;
        next(null, fileTemp);
      });
    }))
    .pipe(rename((path) => {
      const tempPath = path;
      tempPath.basename += '-bundle';
    }))
    .pipe(uglify())
    // .pipe(sourceMaps.write())
    .pipe(gulp.dest(config.paths.dest.js))
    .pipe(liveReload());
};
