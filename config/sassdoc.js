/**
 * @file sassdoc gulp task. Generates docs for scss.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');
const sassDoc = require('sassdoc');

const config = require('./environment');
const options = {
  dest: config.paths.sassDoc,
  display: {
    annotations: {
      function: [
        'description',
        'parameter',
        'return',
        'example',
        'throw',
        'require',
        'usedby',
        'since',
        'see',
        'todo',
        'link',
      ],
      mixin: [
        'description',
        'parameter',
        'content',
        'output',
        'example',
        'throw',
        'require',
        'usedby',
        'since',
        'see',
        'todo',
        'link',
      ],
      placeholder: [
        'description',
        'example',
        'throw',
        'require',
        'usedby',
        'since',
        'see',
        'todo',
        'link',
      ],
      variable: [
        'description',
        'type',
        'property',
        'require',
        'example',
        'usedby',
        'since',
        'see',
        'todo',
        'link',
      ],
    },
  },
  groups: {
    undefined: 'General',
    colors: 'Colors',
    global: 'Global settings',
    gui: 'GUI',
    helpers: 'Helpers',
    'custom-sass-functions': 'Custom Sass functions',
  },
};

module.exports = () => {
  gulp.src(config.paths.glob.scss)
    .pipe(sassDoc(options));
};
