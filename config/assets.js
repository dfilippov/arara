/**
 * @file assets gulp task. Copy layout templates and assets to the build
 *  directory.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');
const liveReload = require('gulp-livereload');

const config = require('./environment');

module.exports = () => {
  gulp.src(config.paths.glob.layout)
    .pipe(gulp.dest(config.paths.build))
    .pipe(liveReload());
  gulp.src(config.paths.glob.assets)
    .pipe(gulp.dest(config.paths.dest.assets))
    .pipe(liveReload());
  // Move favicon.ico to the root:
  gulp.src(`${config.paths.favicons}favicon.ico`)
    .pipe(gulp.dest(config.paths.build))
    .pipe(liveReload());
};
