/**
 * @file scss gulp task. Generate css files and source maps.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

const gulp = require('gulp');
const sass = require('gulp-sass');
const gulpif = require('gulp-if');
const sourceMaps = require('gulp-sourcemaps');

const sassFunctions = require('./sass-functions');
const config = require('./environment');

module.exports = (callback) => {
  gulp.src(config.paths.glob.scss)
    .pipe(gulpif(config.env === 'dev', sourceMaps.init()))
    .pipe(sass({
      includePaths: [
      ],
      functions: sassFunctions,
      outputStyle: 'compressed',
    }).on('error', sass.logError))
    .pipe(sourceMaps.write('.'))
    .pipe(gulp.dest(config.paths.dest.css))
    .on('end', callback);
};
