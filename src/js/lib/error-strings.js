export default {
  invalidEmail: 'Invalid email address.',
  unknownError: `Error: unable to send a confirmation email.
Please, report this error to <a href="mailto:mail@arara.bike">mail@arara.bike</a>.`,
  tooManyRequestsError: `Error: you're sending requests too fast!
Please, try again later.`,
};
