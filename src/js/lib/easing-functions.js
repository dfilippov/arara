/**
 * @file Easing functions for animations.
 * @see {@link https://github.com/gre/bezier-easing|Simple Easing Functions}
 */

/* eslint-disable no-floating-decimal, no-confusing-arrow, no-mixed-operators, no-param-reassign */

export default {
  // No easing, no acceleration.
  linear: t => t,
  // Accelerating from zero velocity.
  easeInQuad: t => t * t,
  // Decelerating to zero velocity.
  easeOutQuad: t => t * (2 - t),
  // Acceleration until halfway, then deceleration.
  easeInOutQuad: t => t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t,
  // Accelerating from zero velocity.
  easeInCubic: t => t * t * t,
  // Decelerating to zero velocity.
  easeOutCubic: t => (--t) * t * t + 1,
  // Acceleration until halfway, then deceleration.
  easeInOutCubic: t => t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1,
  // Accelerating from zero velocity.
  easeInQuart: t => t * t * t * t,
  // Decelerating to zero velocity.
  easeOutQuart: t => 1 - (--t) * t * t * t,
  // Acceleration until halfway, then deceleration.
  easeInOutQuart: t => t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t,
  // Accelerating from zero velocity.
  easeInQuint: t => t * t * t * t * t,
  // Decelerating to zero velocity.
  easeOutQuint: t => 1 + (--t) * t * t * t * t,
  // Acceleration until halfway, then deceleration.
  easeInOutQuint: t => t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t,
};
