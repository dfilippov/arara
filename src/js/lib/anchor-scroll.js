import EventEmitter from './event-emitter';
import animateScroll from './animate-scroll';

export default class AnchorScroll extends EventEmitter {
  constructor() {
    super();
    this.elementsList = [];
    this.hashes = [];
    this.sections = {};
    this.animateHash = {};
    this.state = window.location.hash;
    this.init();
  }

  scrollTo(id, duration, easing) {
    window.cancelAnimationFrame(this.animateHash.rafId);
    this.animateHash = animateScroll(this.sections[id], duration, easing);
  }

  onScroll() {
    super.emit('scroll', Math.min(window.pageYOffset / this.viewportSectionsLength, 1));
    let active = this.state;
    let index = 0;
    for (let i = 0; i < this.hashes.length; i++) {
      if (window.pageYOffset + this.halfWindow >= this.sections[this.hashes[i]]) {
        active = this.hashes[i];
        index = i;
      }
    }

    if (active !== this.state) {
      super.emit('sectionChanged', index);
      this.state = active;
      const hash = this.sections[active] !== 0 ? `#${active}` : '';
      window.history.replaceState({}, document.title, window.location.pathname + hash);
    }
  }

  get elements() {
    return this.elementsList;
  }

  set elements(list) {
    this.elementsList = list;
    for (let i = 0; i < list.length; i++) {
      this.hashes[this.hashes.length] = list[i].id;
    }

    this.calculateOffsets();
  }

  calculateOffsets() {
    this.halfWindow = Math.round(this.elements[0].getBoundingClientRect().height / 2);
    const documentHeight = document.documentElement.scrollHeight;
    let lastFullViewportSection = 0;
    for (let i = 0; i < this.elements.length; i++) {
      let offset = i === 0 ? 0 :
        this.elements[i].getBoundingClientRect().top + window.pageYOffset;
      if (documentHeight - offset >= this.halfWindow) {
        lastFullViewportSection = i;
      } else {
        offset = this.sections[this.hashes[i - 1]] + Math.round(this.halfWindow * 1.5);
      }

      this.sections[this.hashes[i]] = offset;
    }

    this.viewportSectionsLength = this.sections[this.hashes[lastFullViewportSection]];
  }

  init() {
    window.addEventListener('scroll', () => {
      window.requestAnimationFrame(() => {
        this.onScroll();
      });
    });
    let resizing = false;
    window.addEventListener('resize', () => {
      if (resizing) {
        return;
      }

      resizing = true;
      window.requestAnimationFrame(() => {
        this.calculateOffsets();
        this.onScroll();
        resizing = false;
      });
    });
    const cancelAnimation = () => window.cancelAnimationFrame(this.animateHash.rafId);
    window.addEventListener('wheel', cancelAnimation);
    window.addEventListener('touchend', cancelAnimation);
  }
}
