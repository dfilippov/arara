/**
 * @file Smooth page scrolling animation function.
 * @author Daniil Filippov <filippovdaniil@gmail.com>
 */

import Easing from './easing-functions';

export default (targetY, screenDuration, easing) => {
  const currentY = window.pageYOffset;
  const delta = targetY - currentY;
  const duration = (Math.abs(delta) / window.innerHeight) * (screenDuration || 0);

  function step() {
    const t = Math.min((Date.now() - this.startTime) / this.duration, 1);
    const y = this.targetY - ((1 - this.easing(t)) * this.delta);
    window.scrollTo(window.pageXOffset, y);
    if (t !== 1 && this.targetY !== window.pageYOffset) {
      this.lastY = window.pageYOffset;
      this.rafId = window.requestAnimationFrame(this.step);
    }
  }

  const hash = {
    targetY,
    lastY: currentY,
    delta,
    duration,
    easing: Easing[easing] ? Easing[easing] : Easing.linear,
    startTime: Date.now(),
  };

  hash.step = step.bind(hash);
  hash.rafId = window.requestAnimationFrame(hash.step);
  return hash;
};
