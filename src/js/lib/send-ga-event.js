export default (eventName, eventParameters) => {
  if (typeof window.gtag === 'function') {
    window.gtag('event', eventName, eventParameters);
  }
};
