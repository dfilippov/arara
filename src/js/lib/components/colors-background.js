export default class ColorsBackground {
  constructor(root) {
    this.root = root;
    this.svg = {
      portrait: root.querySelector('.show-on-portrait'),
      landscape: root.querySelector('.hide-on-portrait'),
    };
    this.backgrounds = {
      portrait: this.svg.portrait.querySelector('.main'),
      landscape: this.svg.landscape.querySelector('.main'),
    };
    this.patches = {
      portrait: this.svg.portrait.querySelector('.patch'),
      landscape: this.svg.landscape.querySelector('.patch'),
    };
    this.portraitImagesPath = root.getAttribute('data-portrait');
    this.landscapeImagesPath = root.getAttribute('data-landscape');
    this.activePatch = null;
    this.needsPreloading = false;
    this.preloadedImages = {};
  }

  set patch(fileName) {
    this.activePatch = fileName;
    this.redraw();
  }

  get isLandscape() {
    return window.getComputedStyle(this.svg.portrait).display === 'none';
  }

  loadImage({ src, onSuccess = () => {}, onError = () => {}, indicator = true }) {
    const image = new Image();
    if (indicator) {
      this.root.classList.add('loading');
    }

    image.onload = () => {
      this.root.classList.remove('loading');
      this.preloadedImages[src] = image;
      onSuccess();
    };

    image.onerror = (error) => {
      this.root.classList.remove('loading');
      this.preloadedImages[src] = error;
      onError(error);
    };

    image.src = src;
  }

  redraw() {
    const landscape = this.isLandscape;
    const patch = landscape ? this.patches.landscape : this.patches.portrait;
    const background = landscape ? this.backgrounds.landscape : this.backgrounds.portrait;
    if (!background.getAttribute('xlink:href') &&
        !background.hasAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href')) {
      const href = background.getAttribute('data-href');
      background.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', href);
    }

    if (this.activePatch !== null) {
      const path = landscape ? this.landscapeImagesPath : this.portraitImagesPath;
      const src = `${path}/${this.activePatch}`;
      this.loadImage({
        src,
        onSuccess: () => {
          patch.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', src);
          patch.setAttribute('visibility', 'visible');
        },
      });
    } else {
      patch.setAttribute('visibility', 'hidden');
    }

    if (this.needsPreloading) {
      this.preload(this.preloadFiles, landscape);
    }
  }

  preload(files, isLandscape) {
    this.needsPreloading = true;
    if (!this.preloadFiles) {
      this.preloadFiles = files;
    }

    const landscape = typeof isLandscape === 'undefined' ? this.isLandscape : isLandscape;
    const path = landscape ? this.landscapeImagesPath : this.portraitImagesPath;

    files.forEach((fileName) => {
      if (fileName === null) {
        return;
      }

      const src = `${path}/${fileName}`;
      if (!(src in this.preloadedImages)) {
        this.loadImage({
          src,
          indicator: false,
        });
      }
    });
  }
}
