export default class ViewportComponent {
  constructor() {
    this.viewport = document.querySelector('meta[name=viewport]');
  }

  get scrollPosition() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }

  pin() {
    this.oldViewportContent = this.viewport.getAttribute('content');
    const vw = window.innerWidth;
    const vh = window.innerHeight;
    this.viewport.setAttribute('content', `width=${vw}, height=${vh}, initial-scale=1.0`);
  }

  unpin() {
    this.viewport.setAttribute('content', this.oldViewportContent);
  }

  saveScrollPosition() {
    this.oldScrollPosition = this.scrollPosition;
  }

  restoreScrollPosition() {
    window.scrollBy(0, this.oldScrollPosition - this.scrollPosition);
  }
}
