export default class MenuComponent {
  constructor(root) {
    this.root = root;
    this.button = root.querySelector('button');
    this.navigation = root.querySelector('.navigation');
    this.clipped = false;
    this.init();
  }

  close() {
    this.root.classList.remove('opened');
  }

  toggle(event) {
    event.stopPropagation();
    event.preventDefault();
    this.root.classList.toggle('opened');
  }

  update(scrollPosition) {
    if (scrollPosition === 0) {
      if (this.clipped) {
        this.button.classList.remove('clipped');
        this.clipped = false;
      }
    } else if (!this.clipped) {
      this.button.classList.add('clipped');
      this.clipped = true;
    }
  }

  init() {
    const links = this.root.querySelectorAll('a');
    const close = () => this.close();
    const toggle = event => this.toggle(event);
    const stopPropagation = event => event.stopPropagation();

    for (let i = 0; i < links.length; i++) {
      links[i].addEventListener('click', close);
    }

    this.button.addEventListener('touchend', toggle);
    this.button.addEventListener('mouseup', toggle);
    window.addEventListener('keyup', (event) => {
      if (event.code === 'Escape') {
        this.close();
      }
    });

    this.navigation.addEventListener('mouseup', stopPropagation);
    this.navigation.addEventListener('touchend', stopPropagation);
    window.addEventListener('mouseup', close);
    window.addEventListener('touchend', close);
  }
}
