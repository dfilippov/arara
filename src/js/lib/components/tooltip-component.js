export default class TooltipComponent {
  constructor(element) {
    this.element = element || document.createElement('span');
  }

  show() {
    this.element.classList.remove('hidden');
  }

  hide() {
    this.element.classList.add('hidden');
  }
}
