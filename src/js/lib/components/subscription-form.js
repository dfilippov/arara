import EventEmitter from '../event-emitter';
import errors from '../error-strings';
import ga from '../send-ga-event';

export default class SubscriptionFormComponent extends EventEmitter {
  constructor(root) {
    super();
    this.root = root;
    this.email = root.querySelector('.input-field');
    this.button = root.querySelector('.button-rounded');
    this.content = root.querySelector('.content');
    this.messageNode = document.createElement('p');
    this.messageNode.className = 'message';
    this.hasError = false;
    this.init();
  }

  raiseError(message) {
    super.emit('response', {
      message,
      error: true,
    });
  }

  set error(flag) {
    this.hasError = flag;
    if (!flag) {
      this.root.classList.remove('error');
    } else {
      this.root.classList.add('error');
    }
  }

  set message(str) {
    this.messageNode.innerHTML = str.replace(/\n/g, '<br>');
  }

  removeControls() {
    this.content.parentNode.removeChild(this.content);
    this.email.parentNode.removeChild(this.email);
  }

  onSubmit() {
    ga('subscription', { event_label: 'form-submitted' });

    if (!this.email.value.length ||
        (this.email.validity && !this.email.validity.valid)) {
      return this.raiseError(errors.invalidEmail);
    }

    const oldText = this.button.textContent;
    this.button.textContent = 'sending...';
    this.button.disabled = true;
    const xhr = new XMLHttpRequest();
    xhr.open('POST', this.root.getAttribute('action'));
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(`email=${this.email.value}&referrer=${document.referrer}`);
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        this.button.textContent = oldText;
        this.button.disabled = false;
        if (xhr.status === 200) {
          try {
            const json = JSON.parse(xhr.response);
            super.emit('response', json);
          } catch (err) {
            this.raiseError(errors.unknownError);
            ga('subscription', { event_label: 'json-parse-error' });
          }
        } else if (xhr.status === 429) {
          this.raiseError(errors.tooManyRequestsError);
          ga('subscription', { event_label: 'flood-error' });
        } else {
          this.raiseError(errors.unknownError);
          ga('subscription', { event_label: `${xhr.status}-error` });
        }
      }
    };

    return xhr;
  }

  init() {
    this.content.parentNode.insertBefore(this.messageNode, this.content);
    this.email.addEventListener('input', (event) => {
      super.emit('input', event);
      if (this.hasError) {
        this.error = false;
        this.message = '';
      }
    });
    this.root.addEventListener('submit', (event) => {
      event.preventDefault();
      try {
        this.onSubmit();
      } catch (err) {
        this.raiseError(errors.unknownError);
      }
    });
  }
}
