import EventEmitter from '../event-emitter';

export default class AdaptiveVideoComponent extends EventEmitter {
  constructor(root) {
    super();
    this.landscapeVideo = root.querySelector('.video-landscape');
    this.portraitVideo = root.querySelector('.video-portrait');
    this.activeVideo = null;
    this.autoplay = false;
    [this.landscapeVideo, this.portraitVideo].forEach((video) => {
      video.addEventListener('error', () => console.log(video.error));
      video.addEventListener('canplaythrough', () => this.onLoaded());
      // TODO: is it really necessary?
      video.setAttribute('webkit-playsinline', 'webkit-playsinline');
      video.setAttribute('playsinline', 'playsinline');
    });
  }

  onLoaded() {
    if (this.autoplay) {
      this.initAutoplay();
    }
  }

  initAutoplay() {
    const promise = this.activeVideo.play();
    if ('Promise' in window && promise instanceof Promise) {
      promise.catch(error => super.emit('autoplayError', error));
    } else if (this.activeVideo.paused) {
      super.emit('autoplayError', null);
    }
  }

  set landscape(isLandscape) {
    this.activeVideo = isLandscape ? this.landscapeVideo : this.portraitVideo;
    this.load();
  }

  load() {
    this.activeVideo.load();
  }

  play() {
    const promise = this.activeVideo.play();
    if ('Promise' in window && promise instanceof Promise) {
      promise.catch(error => super.emit('playbackError', error));
    }
  }

  pause() {
    this.activeVideo.pause();
  }
}
