import EventEmitter from '../event-emitter';

export default class TapComponent extends EventEmitter {
  constructor(root) {
    super();
    this.root = root;
    this.init();
  }

  init() {
    const touchStart = { x: 0, y: 0 };

    this.root.addEventListener('touchstart', (event) => {
      touchStart.x = event.touches[0].clientX;
      touchStart.y = event.touches[0].clientY;
    }, true);

    this.root.addEventListener('touchend', (event) => {
      event.stopPropagation();
      event.preventDefault();

      if (Math.abs(touchStart.x - event.changedTouches[0].clientX) < 10 &&
          Math.abs(touchStart.y - event.changedTouches[0].clientY) < 10) {
        super.emit('tap', event);
      }
    }, true);

    this.root.addEventListener('mouseup', (event) => {
      event.stopPropagation();
      event.preventDefault();
      super.emit('tap', event);
    }, true);

    this.root.addEventListener('click', event => event.preventDefault(), true);
  }
}
