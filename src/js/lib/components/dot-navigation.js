export default class DotNavigation {
  constructor(root) {
    this.root = root;
    this.outlineStart = root.getAttribute('data-outline-start') * 1;
    this.outlineEnd = root.getAttribute('data-outline-end') * 1;
    this.activeElement = root.querySelector('.active');
    this.outline = root.querySelector('.dots-outline');
    this.links = root.querySelectorAll('.dot');
  }

  set active(element) {
    // Using setAttribute instead of classList for IE11:
    this.activeElement.setAttribute('class', 'dot');
    this.activeElement = element;
    element.setAttribute('class', 'dot active');
    if (element === this.links[this.links.length - 1]) {
      this.root.setAttribute('class', 'dots end');
    } else {
      this.root.setAttribute('class', 'dots');
    }
  }

  updateOutline(percentage) {
    const value = this.outlineStart - ((this.outlineStart - this.outlineEnd) * percentage);
    this.outline.style.strokeDashoffset = value;
  }
}
