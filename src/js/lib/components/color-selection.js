import EventEmitter from '../event-emitter';

export default class ColorSelectionComponent extends EventEmitter {
  constructor(root) {
    super();
    this.root = root;
    this.list = root.querySelector('.colors-list');
    this.buttons = this.list.querySelectorAll('.color');
    this.active = this.list.querySelector('.active');
    this.init();
  }

  get colors() {
    const arr = [];
    for (let i = 0; i < this.buttons.length; i++) {
      arr.push(this.buttons[i].getAttribute('data-image'));
    }

    return arr;
  }

  generateSvgOutline() {
    const delta = (2 * Math.PI) / this.buttons.length;
    // Some magic numbers:
    const radius = 50;
    const buttonRadius = 12.5;
    const strokeWidth = 1.5;
    const scaleFactor = 1.2;

    const url = 'http://www.w3.org/2000/svg';
    const svg = document.createElementNS(url, 'svg');
    svg.setAttribute('version', '1.1');
    svg.setAttribute('viewBox', '0 0 100 100');
    svg.setAttribute('class', 'colors-outline-container');
    const defs = document.createElementNS(url, 'defs');
    const mask = document.createElementNS(url, 'mask');
    mask.setAttribute('id', 'colors-outline-mask');
    const rect = document.createElementNS(url, 'rect');
    rect.setAttribute('x', 0);
    rect.setAttribute('y', 0);
    rect.setAttribute('width', radius * 2);
    rect.setAttribute('height', radius * 2);
    rect.setAttribute('fill', '#fff');
    mask.appendChild(rect);
    for (let i = 0; i < this.buttons.length; i++) {
      const circle = document.createElementNS(url, 'circle');
      circle.setAttribute('cx', (radius * (1 + Math.sin(delta * i))).toFixed(3));
      circle.setAttribute('cy', (radius * (1 - Math.cos(delta * i))).toFixed(3));
      circle.setAttribute('r', buttonRadius * scaleFactor);
      mask.appendChild(circle);
    }
    defs.appendChild(mask);
    const outline = document.createElementNS(url, 'circle');
    outline.setAttribute('class', 'colors-outline');
    outline.setAttribute('cx', radius);
    outline.setAttribute('cy', radius);
    outline.setAttribute('r', radius - strokeWidth);
    outline.setAttribute('mask', 'url(#colors-outline-mask)');
    svg.appendChild(defs);
    svg.appendChild(outline);
    return svg;
  }

  buttonOnClick(event) {
    event.stopPropagation();
    event.preventDefault();

    if (event.target === this.active) {
      return false;
    }

    if (event.type === 'touchend') {
      const touch = event.changedTouches[0];
      const element = document.elementFromPoint(touch.clientX, touch.clientY);
      if (element !== event.target) {
        return false;
      }
    }

    this.active.classList.remove('active');
    this.active = event.target;
    this.active.classList.add('active');

    return super.emit('change', {
      colorName: this.active.textContent.replace(/\s/g, ''),
      fileName: this.active.getAttribute('data-image'),
    });
  }

  init() {
    const handler = this.buttonOnClick.bind(this);
    for (let i = 0; i < this.buttons.length; i++) {
      this.buttons[i].addEventListener('touchend', handler, true);
      this.buttons[i].addEventListener('mouseup', handler, true);
    }
    this.root.appendChild(this.generateSvgOutline());
  }
}
