export default class EventEmitter {
  constructor() {
    this.listeners = {};
  }

  on(label, callback) {
    if (!this.listeners[label]) {
      this.listeners[label] = [];
    }

    this.listeners[label].push(callback);
  }

  emit(label, ...args) {
    const listeners = this.listeners[label];

    if (listeners && listeners.length) {
      listeners.forEach(listener => listener(...args));
    }
  }
}
