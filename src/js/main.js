import './lib/polyfills/request-animation-frame';
import TapComponent from './lib/components/tap';
import MenuComponent from './lib/components/menu';
import ColorSelectionComponent from './lib/components/color-selection';
import ColorsBackgroundComponent from './lib/components/colors-background';
// import SubscriptionFormComponent from './lib/components/subscription-form';
// import TooltipComponent from './lib/components/tooltip-component';
import AdaptiveVideoComponent from './lib/components/adaptive-video';
// import ViewportComponent from './lib/components/viewport';
import DotNavigation from './lib/components/dot-navigation';
import AnchorScroll from './lib/anchor-scroll';
// import errors from './lib/error-strings';
import ga from './lib/send-ga-event';

const menuNode = document.getElementById('menu');
const menu = new MenuComponent(menuNode);

const safetyVideoNode = document.getElementById('safety-video');
const safetyVideo = new AdaptiveVideoComponent(safetyVideoNode);
safetyVideo.autoplay = true;

const safetySectionNode = document.getElementById('safety');
const safetySection = new TapComponent(safetySectionNode);

safetyVideo.on('autoplayError', () => {
  safetySectionNode.classList.add('gesture-needed', 'play');
});
safetySection.on('tap', () => {
  if (safetySectionNode.classList.contains('gesture-needed')) {
    if (safetySectionNode.classList.contains('play')) {
      safetyVideo.play();
    } else {
      safetyVideo.pause();
    }

    safetySectionNode.classList.toggle('play');
  }
});

const activeColorName = document.querySelector('.active-color-name');
const colorSelectionNode = document.querySelector('.color-selection');
const colorSelection = new ColorSelectionComponent(colorSelectionNode);
const colorsBackgroundNode = document.querySelector('#colors .fullscreen');
const colorsBackground = new ColorsBackgroundComponent(colorsBackgroundNode);
colorSelection.on('change', (event) => {
  activeColorName.textContent = event.colorName;
  colorsBackground.patch = event.fileName;
});

const dotsNode = document.querySelector('.dots');
const dotNavigation = new DotNavigation(dotsNode);
const viewportSections = document.querySelectorAll('.viewport-section, .main-footer');
const anchorScroll = new AnchorScroll();
anchorScroll.elements = viewportSections;
anchorScroll.on('scroll', (percentage) => {
  dotNavigation.updateOutline(percentage);
  menu.update(percentage);
});
anchorScroll.on('sectionChanged', (index) => {
  dotNavigation.active = dotNavigation.links[index];
});

function orientationChange(event) {
  const sectionHeight = parseInt(window.getComputedStyle(viewportSections[0]).height, 10);
  if (sectionHeight < 200) {
    // UC Browser doesn't support viewport units — setting the sections height manually:
    for (let i = 0; i < viewportSections.length; i++) {
      viewportSections[i].style.height = `${window.innerHeight}px`;
    }
  }
  safetyVideo.landscape = event.matches;
  colorsBackground.redraw();
}

const mq = window.matchMedia('(orientation: landscape)');
mq.addListener(orientationChange);
orientationChange(mq);

if (mq.matches && window.innerHeight > 500) {
  // Assume that browser is desktop — preload all colors:
  window.addEventListener('load', () => {
    colorsBackground.preload(colorSelection.colors);
  });
}

// const viewport = new ViewportComponent();
// const subscriptionFormNode = document.querySelector('.subscribe-form');
// const subscriptionForm = new SubscriptionFormComponent(subscriptionFormNode);
// const tooltipNode = subscriptionFormNode.querySelector('.input-field + .tooltip');
// const tooltip = new TooltipComponent(tooltipNode);
// const warnRE = /@(hotmail|live|outlook|yahoo)\./;
// subscriptionForm.on('input', (event) => {
//   if (warnRE.test(event.target.value)) {
//     tooltip.show();
//   } else {
//     tooltip.hide();
//   }
// });
// subscriptionForm.on('response', (event) => {
//   subscriptionForm.error = event.error;
//   subscriptionForm.message = event.message;
//
//   if (event.message !== errors.invalidEmail) {
//     subscriptionForm.removeControls();
//   }
//
//   if (!event.error) {
//     subscriptionFormNode.classList.add('ok');
//     ga('subscription', { event_label: 'successful-subscription' });
//   } else {
//     ga('subscription', { event_label: 'subscription-error' });
//   }
// });
// subscriptionForm.email.addEventListener('focus', (event) => {
//   if (warnRE.test(event.target.value)) {
//     tooltip.show();
//   }
//   // Set fixed viewport size when the Android soft keyboard is opened:
//   viewport.pin();
// });
// subscriptionForm.email.addEventListener('blur', () => {
//   tooltip.hide();
//   // Wait some time after the Android soft keyboard close:
//   window.setTimeout(() => {
//     viewport.unpin();
//   }, 1000);
// });

// Prevent images dragging:
const images = document.querySelectorAll('img');
for (let i = 0; i < images.length; i++) {
  images[i].parentNode.addEventListener('dragstart', event =>
    event.preventDefault(), true);
}

// Handling youtube videos "closing":
const closeVideoLinks = document.querySelectorAll('.lightbox a');
for (let i = 0; i < closeVideoLinks.length; i++) {
  const link = new TapComponent(closeVideoLinks[i]);
  link.on('tap', () => {
    const { contentWindow } = link.root.previousElementSibling;
    contentWindow.postMessage(JSON.stringify({
      event: 'command',
      func: 'pauseVideo',
      args: '',
    }), '*');
  });
}

// Removing #close hashes from the current URL:
const anchors = document.querySelectorAll('a[href="#close"]');
const closeActiveModal = () => {
  window.location.hash = '#close';
  anchorScroll.state = null;
  anchorScroll.onScroll();
};
for (let i = 0; i < anchors.length; i++) {
  const link = new TapComponent(anchors[i]);
  link.on('tap', closeActiveModal);
}
window.addEventListener('keyup', (event) => {
  if (event.code === 'Escape') {
    closeActiveModal();
  }
});

// Smooth sections scrolling:
const links = document.querySelectorAll('a');
for (let i = 0; i < links.length; i++) {
  const hash = links[i].getAttribute('href').split('#')[1];
  if (hash && hash in anchorScroll.sections) {
    links[i].addEventListener('click', (event) => {
      event.preventDefault();
      ga('jumpedTo', { event_label: hash });
      anchorScroll.scrollTo(hash, 220, 'easeInOutCubic');
    });
  }
}
