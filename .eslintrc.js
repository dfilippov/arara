module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  extends: 'airbnb-base',
  env: {
    browser: true
  },
  rules: {
    // 'import/no-unresolved': 0,
    // 'import/no-extraneous-dependencies': 0,
    // 'import/extensions': 0,
    'func-names': 0,
    'no-plusplus': 0,
    'object-curly-newline': 0,
  },
};
